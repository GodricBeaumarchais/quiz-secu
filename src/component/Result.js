import {useState} from "react"
import "./Result.css"
function Result ({def, changeDef}) {

    const [reponse, setReponse] = useState(false)

    var ButtonTxt
    if(reponse){
        ButtonTxt = "Next"
    }
    else{
        ButtonTxt = "Voir réponse"
    }

    const button  = () => {
        if(reponse === false){
          setReponse(true)
        }
        else{
          changeDef()
          setReponse(false)
        }}


    return(
        <div className="result-button-container">
            <div className="result-container">
                
                { reponse ?
                    <div id="results" className="search-results">
                        <p>{def}</p>
                    </div>
                    : null
                }
                
                
            </div>
            <div className="button-container">
                <button className="button" onClick={() => button()} >
                    <p className="button-txt" >
                        {ButtonTxt}
                    </p>
                </button>
            </div>
            
    </div>
    )
}

export default Result 