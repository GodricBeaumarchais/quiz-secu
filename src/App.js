import './App.css';
import Result from './component/Result';
import {useState} from "react"

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function App() {

  const liste = require("./def.json");
  const [def, setDef] = useState(liste[1])
  
  const changeDef = () => {
    setDef(liste[getRandomInt(liste.length)])
  } 
  

 
  return (
    <div className="App">
      <header className="App-header">
        <div className='def-container'>
          <p className='def-txt'>{def.name} :</p>
        </div>
        <div className='result-container'>
          <Result def={def.def}  changeDef={changeDef}/>
        </div>
      </header>
    </div>
  );
}



export default App;